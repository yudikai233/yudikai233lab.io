---
title: About me
description: "Description page for the author of the website"
weight: 1
---

# About me

<a href="me.jpg" data-lightbox="myimage" data-title="My caption">
    <img src="me.jpg" alt="me"/>
</a>



{{< tabs "uniqueid" >}}

{{< tab "Chinese" >}}

## 基本信息

俞迪恺
dikai.yu@aalto.fi



## 教育背景

### 2021.09至今
{{< columns >}}
硕士，阿尔托大学，芬兰埃斯波<br>
协同工业设计硕士在读（设计学院）<br>
GPA：4.6/5<br>

<--->

硕士，同济大学，中国上海<br>
工业设计硕士在读（上海国际设计创新学院）<br>
百分制平均成绩：91.55/100<br>
专业排名第一<br>
{{< /columns >}}

### 2017.09 - 2021.06
本科，湖南大学，中国长沙<br>
工业设计学士学位（设计艺术学院）<br>
GPA：3.89/4  百分制平均成绩：91.65/100<br>
专业排名第一

## 语言能力

IELTES academic 7.5
Listening 8.5    Reading 9.0   Writing  6.0   Speaking  6.5


## 奖项与成果

`2023.08.02` Airbit：戴森设计奖 中国区8强<br>
`2023.07.25` Airbit： DIA 中国设计制造大奖决赛入围<br>
`2023.07.10` Airbit：Soft Robotics and Programmable Materials for Human-Computer Interaction ACM DIS'23 Conference, Pittsburgh, PA, USA–Audience Choice Winners–Video category: 1st Place<br>
`2023.05.23` Airbit：iF Design Student Award short-listed of 300
<br><br/>
`2022.11.25` HUAWEI TECH ARENA 3D PRE-CHRISTMAS HACK 第二名（second prize）<br>
`2022.08.16` Pocket Gym： Golden Pin Design Concept Award<br>
`2022.06.29` Pocket Gym： Red dot Design Concept Award<br>
`2022.06.29` SOS Safety Protection System for Railway Workers： Red dot Design Concept Award Best of the Best
<br><br/>
`2023.03.01` 同济大学 2021-2022学年国创研究生奖学金设计创新奖<br>
`2022.10.10` 同济大学 2021-2022学年硕士研究生国家奖学金<br>
<br><br/>
`2022.08.21` 2022湖南省大学生工业设计大赛一等奖<br>
`2021.12.01` 湖南省大学生工业设计大赛二等奖<br>
`2021.06.06` 上海交通大学FourC 24小时设计挑战赛一等奖
<br><br/>
`2020.11，2019.11，2018.11` 三项湖南大学本科生国家奖学金（全本科阶段）<br>
`2019.11` 湖南大学三好学生标兵<br>
`2019.11`第七届惠而浦国际家用电器工业设计大赛金奖<br>
`2018.11` 长沙市图书馆创战记大赛第四名，创新青年奖，优胜奖

## 实践经历

### 实习与工作经历

`2021.10` 联合创始人，参与创始xbotpark常州孵化器极时科技团队项目<br>
`2022.04 - 07`  智己汽车产品与用户触点部门产品经理岗实习<br>
`2022.10.17 - 2023.07.21`  Aalto SCI Logistics Research (Siavash Khajavi Haghighat负责)助理研究员<br>
`2023.07.21至今` Detectium Oy industrial designer and 3D simulation expert (part-time)<br>

### 项目经历

#### 2023
Airbit - an innovation-demoralizing toolkit for soft robotics<br> 
`创始人，全栈设计开发`<br> 
https://yudikai233.gitlab.io/project/airbit/<br> 

Detectium Oy (Finland) 火灾检测与预警解决方案<br> 
`数字孪生环境扫描&搭建，火灾预警与定位算法，火焰识别训练集生成，传感器工业设计`<br> 
https://yudikai233.gitlab.io/project/detectium/<br> 

芬兰Heureka博物馆 - Aalto Design Factory 月球之旅展览<br> 
`月表环境建模&加工，月球车工业设计，操控延时体验算法`<br> 
https://yudikai233.gitlab.io/project/pdpheureka/<br> 

#### 2022
智己赛博座舱内饰与HMi前膽设计<br> 
`内饰设计，灯光布局，中控台工业设计，动画制作&视频渲染`<br> 
https://yudikai233.gitlab.io/project/industrialprojects/#im-car<br> 

同济大学 - 东华大学 奥运射箭&射击项目运动员服装系统<br> 
`样式与布局设计，图案设计，数字模型制作`<br> 

阿斯顿马丁Studio：智能织物<br> 
`2.5图形交互设计与代码实现，中控台工业设计，动画制作&视频渲染`<br> 
https://yudikai233.gitlab.io/project/breathing-light/<br> 

常州xbotpark孵化器即时科技数字阻力健身产品项目<br> 
`联合创始人，Pocket gym及相关产品工业设计，产品战略，用户研究，设计&工程管理`<br> 
https://yudikai233.gitlab.io/project/pocketgymfollowingdesigns/<br> 

#### 2021
德赛西威-湖南大学未来智慧座舱探索项目<br> 
`独立完成《路影随行》概念&原型全栈设计开发`<br> 
https://yudikai233.gitlab.io/project/ridetomovie/<br> 

三一重工-湖南大学 重工机械系列工业设计项目<br> 
`多个提案量产，三一重工系列外饰／内饰／涂装设计，数字模型制作，渲染`<br> 
https://yudikai233.gitlab.io/project/industrialprojects/#sany<br> 

赛特莱特-湖南大学 自行车灯系列产品工业设计项目<br> 
`提案已量产，工业设计，数字模型制作，产品展示视频制作`<br> 
https://yudikai233.gitlab.io/project/industrialprojects/#satelite<br> 

中车 - 湖南大学 盾构机人机分析与设计项目<br> 
`盾构机驾驶室人机工程分析（负责人），人机布局与交互优化设计，数字模型制作`<br> 

#### Before 2021
喜盈门 - 湖南大学 线下商城门店AR体验项目<br> 
`舒达门店环境扫描&AR小程序制作，商城AR体验概念设计与视频制作`<br> 
https://yudikai233.gitlab.io/miniworks/videoworks/#ar-shop-experience<br> 

百度 - 湖南大学 无人出租车车内人机交互探索<br> 
美的 - 湖南大学 数据驱动的趋势研究及应用<br> 
华为 - 湖南大学 二合一平板项目<br> 
海信 - 湖南大学 智能屏概念设计项目<br> 

smartX halo产品展示模型与动画渲染<br> 
`已上线，数字模型制作与渲染`<br> 
https://www.smartx.com/halo/<br> 

Filter Leaf 口罩消毒盒工业设计项目<br> 
`提案已量产，工业设计，数字模型制作，渲染`<br> 
https://yudikai233.gitlab.io/project/industrialprojects/#filter-leaf<br> 


## 专业技能
工业设计建模：Fusion360，Rhino&Grasshopper，solidworks，Alias<br> 
PCB设计：KiCAD   PCBmiling：copper CAM  贴片焊接<br> 
实体制造：3D 打印 `可维护和改装FDM工艺机型`，三轴CNC加工`木材，蜡等`，模具设计与制造`铝，硅胶，蜡，PLA等`，硅胶铸模，注塑<br> 

多边形建模：Blender<br> 
渲染：blender eevee & cycles, keyshot, ember gen, unreal Engine，AE VFX<br> 
Web: Ul&UX设计 Figma & Figjam，p5.js互动图形编程，hugo静态网站搭建<br> 
游戏设计／三维环境模拟：unity3D，unreal Engine<br> 

{{< /tab >}}



{{< tab "English" >}}

## Basic Information (Updated on August 13, 2023)
Dikai Yu, male, from Jinhua, Zhejiang, China, graduated with a Bachelor's degree in Industrial Design from Hunan University's School of Design Arts. Currently, I am studying a joint Master's degree in Industrial Design at Tongji University's Shanghai International College of Design Innovation and Aalto University's Coid Programme(Finland), I am also a Research Assistant at Aalto. I will finish my Master's degrees in June 2024.
- Contact Information: dikai.yu@aalto.fi
- WeChat: wxid_uomdpezuqb4r22

<a href="wechat.jpg" data-lightbox="myimage" data-title="My caption" >
    <img src="wechat.jpg" alt="wechat" width=50% />
</a>

<br><br/>
- **91.55/100** - Tongji University Degree Course Average Score (until 2023-08-13)
- **4.6/5** - Aalto GPA(until 2023-08-13)
- **IELTS 7.5** -Language Certification 2021

---

## Self Introduction
Hi, I'm Dikai Yu.




---

## Achievements and Experiences(since 2021)
### Awards
- 2023-08-02-Airbit: Dyson Design Award China Region Top 8
- 2023-07-25-Airbit: DIA China Design Manufacturing Award Finalist
- 2023-07-10-Airbit: Soft Robotics and Programmable Materials for Human-Computer Interaction ACM DIS'23 Conference, Pittsburgh, PA, USA–Audience Choice Winners–Video category: 1st Place
- 2023-05-23 Airbit: iF Design Student Award short-listed of 300
<br><br/>
- 2022-11-25-HUAWEI TECH ARENA 3D PRE-CHRISTMAS HACK Second Prize
- 2022-08-16-Pocket Gym: Golden Pin Concept Design Award
- 2022-06-29-Pocket Gym: Red Dot Award Design Concept
- 2022-06-29-SOS Safety Protection System for Railway Workers: Red Dot Award  Design Concept best of the best
<br><br/>
- 2023-03-01 Tongji University 2021-2022 Academic Year National Innovation Graduate Scholarship for Design Innovation
- 2022-10-10 Tongji University 2021-2022 Academic Year Master's Research Student National Scholarship
<br><br/>
- 2022-08-21-First Prize in the 2022 Hunan Provincial College Student Industrial Design Competition
- 2021-12-01-Second Prize in the Hunan Provincial College Student Industrial Design Competition
- 2021-06-06-First Prize in the Shanghai Jiaotong University FourC 24-Hour Design Challenge

### Industrial Experience
- 2021-10-01-Co-founder, participated in founding xbotpark Changzhou Incubator Extreme Time Technology Team project

- 2022-04-26-Interned in Zhiji Auto's Product and User Touchpoint Department as Product Manager

- 2022-10-17-Assistant Researcher in SCI Logistics Research (Siavash Khajavi Haghighat)


{{< /tab >}}


{{< /tabs >}}





