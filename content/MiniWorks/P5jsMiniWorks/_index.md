---
title: P5.js mini works
weight: 1
---

# P5.js mini works

<iframe src="https://h5.pinwall.cn/27844/114507/f/index.html" width=100% height =400></iframe>




## Intro

This page documents some p5.js programs I coded, along with their introductions. Most of the programs are available for live demonstration. P5.js is an accessible HTML canvas editing library, suitable for artists engaging in creative coding for new media art, as well as compiling some computer graphics principles and structures. The content on this page is based on the teachings of Aalto fablab Krisjanis, Professor Kostas Terzidis of Tongji University's algorithm design course, Professor Yuan Xiang's p5.js introductory course at Hunan University, and some personal projects. The projects are arranged in chronological order.

---
## a simple metal detector

Based on a Hall sensor array, Xiao RP2040, and p5.js, this work can detect the magnetic field shape of basic metals and map it to a simple two-dimensional graphic. Detailed introduction can be found on the detailed page.

View in [Digital Fabrication - InputDevice Page](https://yudikai233.gitlab.io/test001/InputDevice.html)

---
## knob of sport

This page is a sports data recording prototype, serving the pocket gym project. Interact with the knob prototype on the screen by moving the mouse, with three different data display modes. Horizontal mouse movement switches modes, while vertical movement changes values.

https://editor.p5js.org/yudikai233/sketches/9bX5SeWpC

---
## Blingding Light Code

This page serves the Breathing Light project and is the control interface for the car's central control projection. Use mouse movement to control cursor movement within the sector interface, clicking the mouse to trigger falling light spots at the top. This page also has a version controlled by music melody and beat.
<!-- <iframe src="https://editor.p5js.org/yudikai233/full/1rS7obUeG" width=100% height =600></iframe> -->

https://editor.p5js.org/yudikai233/sketches/9bX5SeWpC

---
## box painter

On this page, you can draw repetitive patterns based on pixel grids and random line brushes using the mouse.

https://editor.p5js.org/yudikai233/sketches/4H_YCci_p

---
## phone painter

Phone Painter uses the box painter's brush and utilizes the phone's gyroscope as input via socket.io. Shaking the phone and flicking towards the screen produces a paint-splatter effect. Unfortunately, due to COVID-19, this work has not been demonstrated on a large screen.

https://oss.pinwall.cn/video/ixfh7WHKGDRHNM7cNHzN.mp4?OSSAccessKeyId=LTAI5t6aBnqjgyJ9QiZShGEb&Expires=1691785602&Signature=GoEEa0wO6QlLG%2B%2FDilM%2FSfFc6aA%3D

---
## phone position clock

Phone Position Clock is another work based on the phone's gyroscope, mapping the gyroscope's three-axis data to the hour, minute, and second hands, creating a spatial-temporal visualization.

https://oss.pinwall.cn/video/CfKaiB43PAzy8s2wWkBZ.mp4?OSSAccessKeyId=LTAI5t6aBnqjgyJ9QiZShGEb&Expires=1691785602&Signature=wxsKpTlnhgD0rauBywv7b5EyDk0%3D

---
## environment generator

This work showcases the basic part of an environment generator design, capable of generating different combinations and arrangements of green plants.
Project Introduction: (pdf)

Final: https://editor.p5js.org/yudikai233/sketches/zuU9Zi1pK

https://editor.p5js.org/yudikai233/sketches/q9l_InSjp

https://editor.p5js.org/yudikai233/sketches/nxEjYj7VT

---
## face daily

This page displays the creation and design process of a mood calendar, using different basic shapes to represent various emotional states.

face daily: https://editor.p5js.org/yudikai233/sketches/RmzwIBLva

draft: https://editor.p5js.org/yudikai233/sketches/N5qIKgDQq

## how to point a circle



These basic examples explore drawing shapes using basic coordinate algorithms rather than pre-set libraries.

how to point a circle: https://editor.p5js.org/yudikai233/sketches/QOlSdc-cZ

how to point a circle2: https://editor.p5js.org/yudikai233/sketches/HRTA-zKub

how to point a circle3: https://editor.p5js.org/yudikai233/sketches/aTvDeuKRQ

---

## detect one ball in black and white

This work explores the concepts of "presence" and "absence." When the black ball is on the left, it is invisible, but when it collides with white particles, its coordinates are exposed below. When the black ball moves to the right, it becomes visible but does not exist for other black particles on the right, nor does it affect their movement. Thus, the visibility and invisibility of the ball in the two areas are relative to different observers.

<iframe src="https://editor.p5js.org/yudikai233/full/cVqWQv51p" width=100% height =400></iframe>

https://editor.p5js.org/yudikai233/sketches/cVqWQv51p

---
## fish in brush painting pool

This prototype displays an interactive ink-style fish pond. Mouse clicks can trigger ripples, attract fish, or scatter them.

<!-- <iframe src="https://h5.pinwall.cn/27844/114507/f/index.html" width=100% height =500></iframe> -->
https://h5.pinwall.cn/27844/114507/f/index.html

---
## paint in the sky series

https://pinwall.cn/project/16438

https://pinwall.cn/project/17254

---
## 2D gen

https://pinwall.cn/project/16434

https://pinwall.cn/project/17246

---
## ellipse battle

https://pinwall.cn/project/16433
