---
title: Video Works
weight: 2
---

# Video Works

## Intro

This page presents some video works I did out of interest or for a certain project. They are bascially CGi or VFX based.

---
## The World of Bilibili
{{<youtube Xl7bKUNQuGs>}}<br>
Created to commemorate Bilibili's 13th anniversary, the video includes 13 different Bilibili small TV skins and scene renderings. Some materials are sourced from Sketchfab.
Content temporarily unavailable outside the Feishu document.

---
## Mid-autumn

{{<youtube udx8_E5Hrtk>}}<br>
This video references the poem "Water Melody: When Will the Bright Moon Appear?" and was created for the Mid-Autumn Festival in 2021. After its release, it received many views, and many viewers used it as a backdrop for reciting the same-named poem. During the Mid-Autumn Festival in 2022, it was cited by Tencent for recruitment purposes.

---
## AR shop experience
{{<youtube gtqNL1F1It0>}}<br>
This video was created during the Xiyinmen - Hunan University AR design project, utilizing images and 3D scanning data collected from the Xiyinmen commercial area as a foundation for AR and VFX creation.
