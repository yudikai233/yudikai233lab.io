---
title: Homepage
description: "Description page for the website"
bookFlatSection: true
weight: 1
---

# Welcome

## Projects
---

<br>
<a href="project/airbit/" data-lightbox="myimage" data-title="My caption">
    <img src="0.png" alt="0.png"/>
</a>
<br></br>
<a href="project/detectium/" data-lightbox="myimage" data-title="My caption">
    <img src="2.png" alt="2.png"/>
</a>
<br></br>
<a href="project/pocketgymfollowingdesigns/" data-lightbox="myimage" data-title="My caption">
    <img src="3.png" alt="3.png"/>
</a>
<br></br>
<a href="project/pdpheureka/" data-lightbox="myimage" data-title="My caption">
    <img src="4.png" alt="4.png"/>
</a>
<br></br>
<a href="project/breathing-light/" data-lightbox="myimage" data-title="My caption">
    <img src="5.png" alt="5.png"/>
</a>
<br></br>
<a href="project/ridetomovie/" data-lightbox="myimage" data-title="My caption">
    <img src="6.png" alt="6.png"/>
</a>
<br></br>
<a href="project/industrialprojects/" data-lightbox="myimage" data-title="My caption">
    <img src="7.png" alt="7.png"/>
</a>
<br></br>
<a href="project/w-manager/" data-lightbox="myimage" data-title="My caption">
    <img src="8.png" alt="8.png"/>
</a>
<br></br>
<a href="project/otherprojects/" data-lightbox="myimage" data-title="My caption">
    <img src="9.png" alt="9.png"/>
</a>

## Mini Works
---
<br>
<a href="miniworks/p5jsminiworks/" data-lightbox="myimage" data-title="My caption">
    <img src="10.png" alt="10.png"/>
</a>
<br></br>
<a href="miniworks/videoworks/" data-lightbox="myimage" data-title="My caption">
    <img src="11.png" alt="11.png"/>
</a>

## Digital Fabrication
---
<br>
<a href="https://yudikai233.gitlab.io/test001/home1.html" data-lightbox="myimage" data-title="My caption">
    <img src="12.png" alt="12.png"/>
</a>