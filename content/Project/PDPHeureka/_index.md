---
title: PDP Heureka
weight: 35
---

# PDP Heureka

## Overview

PDP (Product Development Project) is a comprehensive course at Aalto University aimed at design, engineering, and business majors. Typically lasting eight months (working hours), students in this course form teams to work with sponsors to solve their specific needs. Each student team receives up to €100,000 in funding for solution development.

The PDP team I was part of undertook a project for the Finnish Heureka Natural Science Museum for the design and development of a movable lunar rover and lunar terrain exhibit targeted at children. The core problems to be solved included the development of the lunar rover, the production of the lunar model, and the simulation of signal delay transmitted to the lunar rover. In this project, my responsibilities included scene concept design, digital modeling of the lunar surface and CNC planning, lunar rover industrial design and model making, and implementing code to simulate delayed signals.

---
## About
- Group photo:

<a href="2.jpg" data-lightbox="myimage" data-title="My caption">
    <img src="2.jpg" alt="7"/>
</a>

- Exbition Poster made by me:

<a href="poster.jpg" data-lightbox="myimage" data-title="My caption">
    <img src="poster.jpg" alt="7"/>
</a>

---

## Process & Detailed Intro
<br>

### concept

The concept of the exhibition revolves around the lunar rover. The problems we faced were how to design the scale of the terrain and lunar rover, the layout of the terrain and objects, and how to enable teenagers and children to interact with the lunar rover while viewing the exhibition, leaving a profound impression and interest in space exploration. I created a virtual terrain and lunar rover model and previewed the camera's view within this environment.

During the midterm of the project, we presented a preliminary prototype equipped with a camera and basic signal delay code. I made a statement under the real-time video demonstration.

{{<youtube fViyC0FXKbU>}}

{{<youtube 5l3g6NJJpFI>}}

### landscape

Creating the lunar model was a more complex process than it appeared, as we needed to create a 3m x 3m foam prototype that could bear the weight of the lunar rover model and naturally form a barrier to prevent the lunar rover from crossing (as children always have the urge to explore boundaries). Moreover, our prototype had to be machined from 15 foam boards, each 0.6m x 2.4m, with minimal material waste. The finished product also had to be split in half by a natural seam for easy transport. In short, it was a highly grounded and restrictive environment model creation, and I managed to achieve it.

I first mapped and sculpted the terrain in Blender based on NASA's lunar height map, converted it into a CNC-processable nurbs file format through Rhino, and then arranged and split the 15 foam boards in Fusion360, calculating the toolpath. After machining on a vare CNC, the final physical model was obtained with Thomas's handling and refinement.

<a href="6.jpg" data-lightbox="myimage" data-title="My caption">
    <img src="6.jpg" alt="6"/>
</a>

<a href="10.png" data-lightbox="myimage" data-title="My caption">
    <img src="10.png" alt="10.png"/>
</a>


<br></br>

<a href="11.jpg" data-lightbox="myimage" data-title="My caption">
    <img src="11.jpg" alt="11"/>
</a>



<a href="33.jpg" data-lightbox="myimage" data-title="My caption">
    <img src="33.jpg" alt="22"/>
</a>

### rover exterior

When creating the lunar rover's exterior, our budget no longer allowed for a perfect model made by a model factory. So I decided to do my best within the range attainable by FDM 3D printing. Based on the printing range of the Ultimaker S3, I designed the split structure of the lunar rover's exterior and heat-dissipation frame, using silver and black materials for printing and assembly. The final product was paired with the structure of the lunar rover, achieving satisfactory results.

{{<youtube KPCuvK5iKtM>}}

<a href="55.jpg" data-lightbox="myimage" data-title="My caption">
    <img src="55.jpg" alt="55"/>
</a>

<a href="44.jpg" data-lightbox="myimage" data-title="My caption">
    <img src="44.jpg" alt="44"/>
</a>

<a href="22.jpg" data-lightbox="myimage" data-title="My caption">
    <img src="22.jpg" alt="22"/>
</a>
