---
title: Detectium
weight: 20
---

# Detectium

## Overview

Detectium is a research group and entrepreneurial project led by postdoctoral fellow Siavash, with which I have been continually involved during my time at Aalto. The goal of the project is to create a fire detection, early warning, and analysis system targeting B2B and government sectors. Specifically, it consists of a flame detection sensor terminal, an ML-driven flame detection/localization algorithm, and a Unity WebGL-based three-dimensional digital twin environment on Azure. The system is currently being adopted by some companies and government institutions in Northern Europe, Eastern Europe, and Central Asia.

In this project, my contributions include the construction of Unity scenes, data processing, the creation of flame detection ML training sets, and the industrial design of the sensors. More details can be found under "Process & Detailed Intro," where sensitive customer environment images have been blurred.

---
## About

{{<youtube LEkIU2oGKjU>}}

homesite: https://detectium.io/
- The project starts from October 2022 to Present.

---
## Process & Detailed Intro
<br>

### Unity Environment for fire sensing

After determining the environment where the sensors need to be placed, using the factory in the image below as an example, we first use LiDAR for high-precision three-dimensional scanning of the environment, followed by processing the scanned data in 3D editing software. Then, through low-polygon reconstruction, we create a digital 3D environment, applying real-world texture data. Later, using a detection area percentage algorithm (developed by me in Unity using C#), we calculate and optimize the virtual sensor's position and quantity. Then, we align real sensors based on the virtual ones. When real-world sensors detect a flame, a virtual flame of corresponding temperature and size is generated at the corresponding location in the virtual environment, simulating the fire's spread over time to provide early warnings and reference for response.

- 3d scaned data(blured)：

<a href="7.png" data-lightbox="myimage" data-title="My caption">
    <img src="7.png" alt="7"/>
</a>


- Model rebuilded：

<a href="5.jpg" data-lightbox="myimage" data-title="My caption">
    <img src="5.jpg" alt="5"/>
</a>

- Fire detection example：



<a href="2.jpg" data-lightbox="myimage" data-title="My caption">
    <img src="2.jpg" alt="2"/>
</a>

<a href="9.png" data-lightbox="myimage" data-title="My caption">
    <img src="9.png" alt="9.png"/>
</a>

### Fire randomizer generator for ml

The sensor terminal consists of an RGB camera and an IR camera, where the IR camera identifies high-temperature areas and positions, and the RGB camera detects and identifies types of flames. The ML algorithm on the RGB camera is based on YOLO, trained with a set composed of hundreds of environmental photos processed using p5.js and over 30 different types of flame models generated using ember gen. A single training run generates a 5s flame video, and using an algorithm I wrote, thousands of realistic flame videos meeting standards can be generated in 2 hours through a single browser page, greatly accelerating the training speed of the ML model.

{{<youtube 7jQ7UUKlOrM>}}<br>



### sensor workable prototype Industrial design & manifacture

The industrial design of the sensor is a result determined by factors such as product shape, structure, and a modeling approach suited for 3D printing. Key considerations include heat dissipation and moisture resistance, minimizing internal space while leaving room for wiring, and designing fixed structures for the two cameras. The industrial design consists of a housing for the Raspberry Pi and cameras and a wall-mounting structure connected to the housing. After more than ten iterations, six functional prototypes were manufactured using the most cost-effective FDM 3D printing, and they were placed in laboratories and actual testing sites.

<a href="3.jpg" data-lightbox="myimage" data-title="My caption">
    <img src="3.jpg" alt="3"/>
</a>

{{<youtube rbdamdSfJTU>}}<br>

sensor testing(blured)：

<a href="8.png" data-lightbox="myimage" data-title="My caption">
    <img src="8.png" alt="8"/>
</a>