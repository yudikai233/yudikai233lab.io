---
title: Industrial Projects
weight: 60
---

# Industrial Projects



## Intro

This page is a documentation of the Industrial Projects I have done.

- The order of content is based on time created

---
## Huawei Hackathon 2022

A two-and-a-half-day Huawei hackathon held in Helsinki, Finland, in December 2022. The goal was to create interactive 3D dynamic wallpapers using Huawei's self-developed graphics editing engine and related tools. 

{{<youtube aZGuE1ZNq6A>}}

---
## iM car

The work related to iM Car includes the internship phase from May to August 2022 and the design project phase in November 2022. The main achievement of the internship phase was the exploration of future HMi design principles. The design project phase unveiled the concept of the next generation of interiors, lighting, and vehicle interfaces

### iM NextGen interior & light layout


{{<youtube Phu8aqqePBk>}}


### iM HMI

---
## SANY 

The SANY - Hunan University project encompasses almost the entire series of SANY's manned heavy machinery exterior and interior design. This entry lists some of my selected or mass-produced designs.

<a href="SANYinterior1.png" data-lightbox="myimage" data-title="My caption">
<img src="SANYinterior1.png" alt="SANYinterior1.png"/>
</a>

<a href="SANY1.png" data-lightbox="myimage" data-title="My caption">
<img src="SANY1.png" alt="SANY1.png"/>
</a>

<a href="SANY2.png" data-lightbox="myimage" data-title="My caption">
<img src="SANY2.png" alt="SANY2.png"/>
</a>

<a href="SANY3.png" data-lightbox="myimage" data-title="My caption">
<img src="SANY3.png" alt="SANY3.png"/>
</a>

---


## Satelite

Satellite is a bicycle light manufacturer whose products consistently maintain high sales on Amazon. This entry showcases the mass-produced products I designed and their design process.

<a href="S1.png" data-lightbox="myimage" data-title="My caption">
<img src="S1.png" alt="S1.png"/>
</a>

<a href="S2.png" data-lightbox="myimage" data-title="My caption">
<img src="S2.png" alt="S2.png"/>
</a>


---
## Filter leaf

Filter Leaf is a mask disinfection box manufacturer that emerged at the onset of the pandemic. The products are mainly sold in Japan. This entry showcases the mass-produced products I designed and their design process.

<a href="F1.png" data-lightbox="myimage" data-title="My caption">
<img src="F1.png" alt="F1.png"/>
</a>