---
title: Other School Projects
weight: 80
---

# Other School Projects



## Intro

This page documents some other school projects, including those at Aalto University, Tongji University, and Hunan University, during my study period. Each project is displayed with images, videos, or documents.

- The order of content is based on time created

---
## AR Bustop nav

AR Bus Stop Navigation is a mobile AR interactive prototype made for the Finnish HSL bus system. Users can use it at bus stops to view real-time route information, station names, and arrival information and locations. The prototype was developed using Unity and Vuforia.
Unfortunately, this content cannot be displayed outside of Feishu documents.

{{<youtube OfVMKP-SrIk>}}

---
## UID konwing & Making

The user-inspired design series of courses is taught by Prof. Turrka of Aalto. In class, I wrote the following papers based on some user participatory design projects and experiments that I participated in (recommended to download and read).

<a href="/fullscreen-pdf" target="_blank">
  <embed src="UID.pdf" type="application/pdf" width="100%" height="500" alt="pdf" pluginspage="http://www.adobe.com/products/acrobat/readstep2.html">
</a>

---
## SFC water

This project was created in 2021, and its focus is on the future water circulation system and visualization at the Sino-Finnish Centre of Tongji University. The video imagines a "water energy" interface based on a head-mounted augmented reality device.
Unfortunately, this content cannot be displayed outside of Feishu documents.

{{<youtube ZQWVa2-YM2M>}}<br>

---
## CHAMI

"Chami" is a compound word for "chat me." The design goal of this application is to address the needs of international students (especially females) in domestic universities to become familiar with unfamiliar environments, solve life difficulties, and social needs, to create a sense of belonging in an unfamiliar environment.
Unfortunately, this content cannot be displayed outside of Feishu documents.

{{<youtube 43nWtSuQzIg>}}<br>

---
## Fellow Space

Fellow Space is a futuristic personal perception vehicle design, conceptualized in the design studio course taught by Professors Yang Wenqing and Fu Lizhi at Tongji University. The vehicle is equipped with deformable force arms and fabric tentacles, allowing it to create various shapes within the user's reachable space.
Unfortunately, this content cannot be displayed outside of Feishu documents.

<!-- <iframe src="//player.bilibili.com/player.html?aid=850786375&bvid=BV11L4y147V4&cid=488868549&page=1&autoplay=0" width=100% height =400 ></iframe> -->
{{<youtube ISyHQiJiLeM>}}

---
## DJi flying car

This project created a non-coaxial quadrotor flying car based on DJI's brand characteristics, aiming to solve the transportation needs for short-distance, low-altitude travel.
Unfortunately, this content cannot be displayed outside of Feishu documents.

{{<youtube M5bNRuw20yI>}}
---

## 3D printing drone

This project was created in 2020 and won the silver award in that year's China Industrial Machinery Design Competition. The design concept is a parent-child drone system that can perform 3D scanning and minimal additive 3D printing on damaged cultural relics and historical sites.

<iframe src="//player.bilibili.com/player.html?aid=289249412&bvid=BV11f4y1z7Jc&cid=299043527&page=1&autoplay=0" width=100% height =400 ></iframe>


---

## Visit more on Pinwall

https://pinwall.cn/users/5702?jobTag=2