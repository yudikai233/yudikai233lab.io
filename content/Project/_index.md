---
title: Projects
description: "Description page for the Projects"
bookFlatSection: false
bookCollapseSection: false
weight: 2
---

<br>
<a href="airbit/" data-lightbox="myimage" data-title="My caption">
    <img src="1.png" alt="1.png"/>
</a>
<br></br>
<a href="detectium/" data-lightbox="myimage" data-title="My caption">
    <img src="2.png" alt="2.png"/>
</a>
<br></br>
<a href="pocketgymfollowingdesigns/" data-lightbox="myimage" data-title="My caption">
    <img src="3.png" alt="3.png"/>
</a>
<br></br>
<a href="pdpheureka/" data-lightbox="myimage" data-title="My caption">
    <img src="4.png" alt="4.png"/>
</a>
<br></br>
<a href="breathing-light/" data-lightbox="myimage" data-title="My caption">
    <img src="5.png" alt="5.png"/>
</a>
<br></br>
<a href="ridetomovie/" data-lightbox="myimage" data-title="My caption">
    <img src="6.png" alt="6.png"/>
</a>
<br></br>
<a href="industrialprojects/" data-lightbox="myimage" data-title="My caption">
    <img src="7.png" alt="7.png"/>
</a>
<br></br>
<a href="w-manager/" data-lightbox="myimage" data-title="My caption">
    <img src="8.png" alt="8.png"/>
</a>
<br></br>
<a href="otherprojects/" data-lightbox="myimage" data-title="My caption">
    <img src="9.png" alt="9.png"/>
</a>
