---
title: Airbit
description: "Description page for the process of deploying and organizing content for Digital Fabrication"
weight: 10
---
# Airbit 

An innovation-demoralizing toolkit for soft robotics

<a href="Main.jpg" data-lightbox="myimage" data-title="My caption">
    <img src="Main.jpg" alt="My image"/>
</a>
<br><br/>
{{<youtube kT10PKsGy6A>}}<br>

## Overview

Airbit is a low-barrier, highly engaging, and expandable soft robotics creative development kit. Its design aim is to enable users of nearly all cognitive levels to learn about soft robotics structures and create prototypes, thereby gaining knowledge and enjoyment from the process. The hardware component of Airbit comprises four fundamental elements: an air source module (Airbrick), a soft body module (Softbody), a connector module (Connector), and a development baseboard (Baseboard), along with their corresponding API interfaces. Users and developers can use these components for prototype design or integrate them into other development projects.

The software part is a multi-platform application that connects with the hardware via WiFi or serial port. It features interactive tutorials, design aids, control programming, and community-sharing functions. From school-age teenagers and new media artists to experienced developers and researchers, Airbit's easy-to-use assembly-based creation method and rich API can meet the varied needs of entertainment, education, creation, and research across different levels and directions. By facilitating democratized creation, Airbit strives to push the development of the soft robotics industry forward.

---
## About
- Timeline:  Dec 2022 -- Present

- [*Soft Robotics and Programmable Materials for Human-Computer Interaction ACM DIS'23 Conference, Pittsburgh, PA, USA*](https://www.softrobotics.io/dis23)--Audience Choice Winners--Video category: 1st Place

- [*Designing Process Documentation*](https://yudikai233.gitlab.io/test001/AboutMe.html)--About the design, Source Documentation

- [*Development Process Documentation*](https://yudikai233.gitlab.io/test001/FinalProjectMaking.html)--About the development, Source Documentation

---
## Detailed Introduction
### Modules
<br>
<a href="AirbitModules1.jpg" data-lightbox="myimage" data-title="My caption">
    <img src="AirbitModules1.jpg" alt="My image"/>
</a>
{{<youtube PMaQocikE44>}}<br>

Airbit possesses four fundamental modules: the air source module (Airblock), the soft body module (Softbody), the connector module, and the development baseboard. Their main functions and introductions are as follows:
#### AirBrick

<a href="Airbrick.png" data-lightbox="myimage" data-title="My caption">
    <img src="Airbrick.png" alt="Airbrick"/>
</a>
The basic air source module is a single-pump source, including a miniature air pump, a solenoid valve, a control circuit with a pressure sensor, a rechargeable battery, and an external Pongopin interface (for connecting external signals and power). The module's casing has a primary outlet and a secondary inlet. In most cases, the soft body module connects to the outlet, which links to the air pump's inflation port, the solenoid valve, and the pressure sensor, enabling control of various modes (inflation, hold, hold & sense, stop) and outlet power. Beyond the single pump air source, there is a four-pump air source that can control more soft body modules, with its four outlets independently controlling air volume and mode, suitable for more complex designs. Air source modules can communicate via WiFi and connect through the connector module for serial or diverging air routes. Each air source module can function independently without reliance on an external power source or be combined with other modules, and can connect to internet-connected control terminals (PC, tablet, smartphone, etc.).

#### SoftBody

<a href="SoftBody.png" data-lightbox="myimage" data-title="My caption">
    <img src="SoftBody.png" alt="SoftBody"/>
</a>
The structure of the soft body module is a key innovation point. Each module consists of two tightly meshed parts: a silicone soft body piece and a hard plastic piece. The silicone soft body piece determines the module's deformation pattern when inflated, while the hard plastic piece serves as a connectable ventilation port, acting as the connection structure with other modules. This design simplifies the operation and enhances the expandability of the soft body module, lowering the entry barrier and operational cost of soft robot prototyping. Soft body modules can plug into air source modules and connector modules, forming a diverse range of soft mechanical prototype designs. Certain specially shaped soft body modules can combine to form objects with free movement directions, like tentacles. Those made with mixed special materials can sense touch and send signals to the air source module.

#### Connector

<a href="Connector.png" data-lightbox="myimage" data-title="My caption">
    <img src="Connector.png" alt="Connector"/>
</a>
Different connector modules can branch, merge, or serially connect air routes to achieve varying soft control effects and quantities. A connector module can be a brick-shaped hard structure or an extendable, deformable tube. Its interface follows the same standards as the air source and soft body module interfaces. The connector modules can combine to form expanded functional connection structures.

#### Baseboard
<a href="BaseBoard.png" data-lightbox="myimage" data-title="My caption">
    <img src="BaseBoard.png" alt="BaseBoard.png"/>
</a>
The development baseboard can connect charging power, external signals, and programs to the air source module. It can also connect multiple air source modules for coordinated control via a bus system. The baseboard supports USB-C serial port protocol and 5V-4.5A power input, and comes with a WiFi antenna. It can connect and communicate with third-party development components (such as Arduino, Raspberry Pi, various sensor and actuator modules, etc.) via APIs. A small baseboard can pair with a single air source module to form a portable development module, suitable for wearable prototype production scenarios.
Diverse Combinations: From pneumatic grippers to biological motion simulation, different Airbit modules can combine into varying application forms to realize a range of functionalities. Users can start learning to build Airbit from simple beginner forms and can also integrate complex functional prototypes with third-party hardware and software.

<a href="Combined2.png" data-lightbox="myimage" data-title="My caption">
    <img src="Combined2.png" alt="Combined2.png"/>
</a>

<br>

### Structure, Making & Prototype
<br>
{{<youtube QPdd-9KLZd0>}}<br>
<a href="StructureMaking1.jpg" data-lightbox="myimage" data-title="My caption">
    <img src="StructureMaking1.jpg" alt="My image"/>
</a>


<a href="Prototype1.jpg" data-lightbox="myimage" data-title="My caption">
    <img src="Prototype1.jpg" alt="My image"/>
</a>

The design and manufacturing of Airbit encapsulates a parallel, iterative process, rooted in user-centric design and prototype-driven development. This paper describes the stages of Airbit's development, beginning with conceptual ideation, the creation of initial models and renderings, through lab development and manufacture, culminating in a set of functional prototypes. Feedback and insights from potential users and experts were incorporated at each stage, refining the design and improving overall functionality.

#### Soft-Rigid Body Structure
<br>
<a href="Soft&RigidStructure.jpg" data-lightbox="myimage" data-title="My caption">
    <img src="Soft&RigidStructure.jpg" alt="Soft&RigidStructure.jpg"/>
</a>
The construction of the soft body components was an intricate process involving comprehensive experimentation. Each soft body module consists of two tightly interlocked parts: a deformable silicone element and a rigid plastic component. The silicone component determines the deformation pattern upon inflation, while the rigid plastic component acts as a connection structure, linking with other modules. This design paradigm provides simplicity in assembly and enhances expandability, thus reducing both the barrier to entry and cost associated with soft robot prototyping. The development and refinement of this structure followed a three-phase strategy: casting experimentation, iterative single-part construction, and fabrication of multiple shapes.


#### Airflow System
<br>
<a href="AirflowSystem.jpg" data-lightbox="myimage" data-title="My caption">
    <img src="AirflowSystem.jpg" alt="AirflowSystem.jpg"/>
</a>
The airflow system constitutes a significant part of Airbit's internal structure and critically determines its operational range and parameters. Initially, a configuration involving two air pumps and four solenoid valves was employed for air output. However, to enhance control granularity and maintain consistent airflow across the system, the design was revised to utilize a single air pump and a single solenoid valve for each individual outlet. After resolving issues related to PCB current, a functional prototype was produced, illustrating the viability of the new design.

#### Driver Board
<br>

<a href="PCB1.jpg" data-lightbox="myimage" data-title="My caption">
    <img src="PCB1.jpg" alt="PCB1.jpg"/>
</a>
<a href="PCB.jpg" data-lightbox="myimage" data-title="My caption">
    <img src="PCB.jpg" alt="PCB.jpg"/>
</a>


The process of designing and manufacturing the driver circuit board was an extensive, iterative procedure that necessitated numerous redesigns and prototyping rounds. The initial goal was to drive two air pumps and four solenoid valves within a four-channel base module. Upon encountering challenges related to current overload, a shift was made to a more resilient driver chip along with component upgrades. This adaptation resulted in the successful creation of a more robust and effective iteration, validating the design methodology.

### UX WIP
<br>
<a href="UX1.jpg" data-lightbox="myimage" data-title="My caption">
    <img src="UX1.jpg" alt="My image"/>
</a>
The software component of Airbit is a multi-platform application that connects with the hardware via WiFi or a serial port. It includes interactive tutorials, design assistance, control programming, and community sharing features.

#### Design & Build
<br>
<video autoplay muted loop id="myVideo" style="width:100%; height:auto;">
  <source src="Design&Build.mp4" type="video/mp4">
  Your browser does not support HTML5 video.
</video>

The design/construction interface presents a three-dimensional environment comprising various Airbit components and a virtual building environment. Users can build prototypes in reality and synchronize them with the digital environment, or complete the design within the application and export the assembly steps. New users can learn how to construct Airbit prototypes through interactive tutorials.

#### Coding & Controlling
<br>
<video autoplay muted loop id="myVideo" style="width:100%; height:auto;">
  <source src="Control&Programming.mp4" type="video/mp4">
  Your browser does not support HTML5 video.
</video>

Upon completion of assembly, users can proceed to the programming/control interface to manage the air source module's airflow control and code writing for actions. Even users without a coding background can design complex movements using a graphical language.

#### Works & Community
<br>
<a href="Community.png" data-lightbox="myimage" data-title="My caption">
    <img src="Community.png" alt="Community.png"/>
</a>

After designing and building a prototype, users can save their digital design documents in their own portfolio or share them with the community, gaining inspiration and knowledge from others. Third-party components and user-made Airbit components can also be uploaded to the community for sharing and reuse. This platform, therefore, enhances not only user engagement but also collective knowledge-building and creativity.
