---
title: W-Manager
weight: 70
---

# W-Manager

<br>
<a href="WManagerOverView.png" data-lightbox="myimage" data-title="My caption">
    <img src="WManagerOverView.png" alt="WManagerOverView.png"/>
</a>
{{<youtube ZJv_BQMK298>}}<br>

## Overview

W-manager mobile food management center is an Internet of things solution for 
high building food material logistics. The original refrigerator in the user's home is 
divided into two parts: door and box.The box is independent as the food material 
storage center. The stored food is the goods allocated from the storage center at 
the bottom of the floor according to the user's orders and data suggestions. One 
box is for the use and consumption of multi-level households

---
## About

A concept product designed for Whirlpool Design Contest 2018, based on the development of 5G and Internet of things in the next 5-10 year.

---
## Process & Detailed Intro



<a href="W1.png" data-lightbox="myimage" data-title="My caption">
    <img src="W1.png" alt="W1.png"/>
</a>

### Design

The size and form of the local refrigerator on the left side can be selected by the user according to their own needs.The glass door panel of W-manager on the right side is installed in the house of the whole building.

On the left is the local refrigerator space, which reduces the width compared with the previous products.

On the right is the glass door connecting the food management 
center, which displays the user information and food related information on the door panel 


<a href="W3.png" data-lightbox="myimage" data-title="My caption">
    <img src="W3.png" alt="W3.png"/>
</a>

### Service Flow

- The user places an order through the refrigerator door or directly calls the mobile food management center

- W-manager transfer food from the storage center at the bottom of the building to the home of the user

- Households purchase goods and exchange materials through the W-manager

- The refrigerator door passes the order information to W-manager

- The underground storage center under direct operation will allocate the types and quantityof 
goods in W-manager according to the user’s order

<a href="W2.png" data-lightbox="myimage" data-title="My caption">
    <img src="W2.png" alt="W2.png"/>
</a>

