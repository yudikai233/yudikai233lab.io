---
title: Breathing light
weight: 40
---

# Breathing light

<br>
<a href="BreathingLightOverview.png" data-lightbox="myimage" data-title="My caption">
    <img src="BreathingLightOverview.png" alt="BreathingLightOverview.png"/>
</a>
{{<youtube 7ivnwi-J6mQ>}}<br>

## Overview

In this project, we de, we develop a car-based pneumatic interaction surface and explore if this kind of HCI interface can enhance  the users' reaction speed and user experience on the sense of companionship, compared to a touch screen based  interface.For this aim, we conducted an experiment where users will go though the same interaction steps on both  devices while driving. One is our pneumatic interface with projection mapping on it, another is a touchable screen with the  same GUI elements. To evaluate the influence on the user experience, users were asked to perform three different tasks  :play a music game, adjust car control settings and drive with motion display on screen. During the experiment,  participants performed a virtual version of the well-known whac-a-mole game, therefore interacting with the virtual  environment, while sitting at a virtual table. These results are supported by time measuring, subjective questionnaires but  also through behavioural responses.Taken together, our results show that the pneumatic interaction surface may create  more sense of companionship compared to screen based GUI system, yet the reaction speed and the influence on user's  attention can be put into further discussion.

<a href="/fullscreen-pdf" target="_blank">
  <embed src="Studio2AstonMartin.pdf" type="application/pdf" width="100%" height="400" alt="pdf" pluginspage="http://www.adobe.com/products/acrobat/readstep2.html">
</a>

---
## About

This Project is based on the course design studion II of Tongji University tutored by Qi Wang and Huasen Zhao

---
## Process & Detailed Intro

### Concept:

{{<youtube v1B_6a3gLyM>}}<br>

In this concept featuring Breathing Lights, the car is designed with a biotic feel. Not only does it simulate the owner's beloved pet through variations in color, temperature, fluctuations, and sound effects, but it also provides real-time feedback based on driving conditions and the owner's commands. This effectively conveys the message, "I am alive," offering the user a warm, emotional, and therapeutic travel experience.

With Breathing Lights, one can fully enjoy the musical ambiance and the joy of creation. It not only combines interior design to provide music lovers with a richer feast of auditory and visual sensations but also enhances the fun of cruising for both the driver and passengers during travel or while parked.

Cars designed with a biotic concept also have their own "consciousness." During the journey, they can search the database for popular routes or scenic spots nearby, freely change the route, and obtain user satisfaction feedback to gather user preferences. This information can be used to proactively suggest more satisfying cruising routes for the next trip.

### Design:

#### Central Control Mode:
- The middle bottom panel is used for detailed control and parameter adjustments. The App List is displayed on both sides, allowing for selection and further adjustment.

<a href="1.png" data-lightbox="myimage" data-title="My caption">
    <img src="1.png" alt="1.png"/>
</a>

#### Music Background Mode:

- Flowing Shadow Mode: The specks of light on the central control surface pulsate to the rhythm of breathing, flowing over the raised and recessed surfaces with color-changing lights.
- Waterfall Mode: Light points pour out like a waterfall. This mode can be triggered by fast driving or musical climax or can be manually activated. Audio signals are translated into different patterns and colors.

{{<youtube kSlVnKh_Rio>}}<br>

<a href="2.png" data-lightbox="myimage" data-title="My caption">
    <img src="2.png" alt="2.png"/>
</a>

#### Music Editing Mode:

- Piano Key Mode: Different tonal areas naturally recess like piano keys under the touch of a finger, with light spots providing feedback.
- String Mode: The area swiped by the finger forms string-like light spots, triggering corresponding tones. Interact with the central control to experience the joy of music creation.

<a href="3.png" data-lightbox="myimage" data-title="My caption">
    <img src="3.png" alt="3.png"/>

### Prototype Plan:
Using LED dot matrix drive chips, an 8x8 matrix can be driven with three IO ports. However, LED driving is based on the principle of visual persistence, while the inflation and deflation of airbags are continuous processes. By cascading, the output IO ports of the Arduino can be increased, theoretically achieving 32x32 3-bit control precision, but the cost is relatively high. In this 32x32 dot matrix area, gray represents inactive protruding points, and black represents active ones. The larger the black area, the higher the protrusion, with each protruding point having 3-bit depth, meaning there are eight different protrusion height states for each point.



<a href="4.png" data-lightbox="myimage" data-title="My caption">
    <img src="4.png" alt="4.png"/>