---
title: Ride To Movie
weight: 50
---

# Ride To Movie

<br>
{{<youtube 89rxvVBzqZU>}}
<a href="InCarCinemaOverview.jpg" data-lightbox="myimage" data-title="My caption">
    <img src="InCarCinemaOverview.jpg" alt="InCarCinemaOverview.jpg"/>
</a>
<br>

## Overview

Ride to Movie, an award-winning concept from the Desay SV-Hunan University Automotive HMI Design Competition, is an innovative passenger vehicle designed for families in 2035. This electric-powered car integrates elements of SUVs and MPVs and leverages advanced Level 4 and Level 5 autonomous driving technology. It optimizes internal space by placing the power system in the chassis and features a unique cinema mode for in-car entertainment.

---
## About

- This concept is based on Desay SV--Hunan University Automotive HMI Design project, and also the graduate design project of my bachelor's degree. Done in June 2021, this project is published on the ZHUANGSHI journal graduation design page.

- Former Concept:

{{<youtube BAoAJqZwfRs>}}

---
## Detailed Introduction

view pdf (better download to view, the pdf is written in Chinese, if you have questions about that, please contact the aurthor dikai.yu@aalto.fi):

<a href="/fullscreen-pdf" target="_blank">
  <embed src="InCarCinema.pdf" type="application/pdf" width="100%" height="300" alt="pdf" pluginspage="http://www.adobe.com/products/acrobat/readstep2.html">
</a>

### Design

<a href="Design1.jpg" data-lightbox="myimage" data-title="My caption">
    <img src="Design1.jpg" alt="Design1.jpg"/>
</a>

#### Interior Design


Ride to Movie's interior layout consists of three rows of seats, dividing the car into distinct driving and passenger areas. The passenger area adopts a semi-enclosed design inspired by living room layouts, while the driver's area is focused on the driver, with clear functional divisions. The steering wheel can retract in autonomous driving mode, and all seats can transform into a flat lounge area for an immersive cinema experience.

<a href="Interior.jpg" data-lightbox="myimage" data-title="My caption">
    <img src="Interior.jpg" alt="Interior.jpg"/>
</a>

#### Advanced Technology for Entertainment

The vehicle is equipped with touch screens running the length of the interior on both sides, accessible whether passengers are seated or lying down. In cinema mode, all screens can enter sleep mode, and the electrochromic film on the screens can power off, turning the glass texture to frosted and black to reduce reflections. This vehicle brings the cinema experience into the car with the added comfort and privacy of home.

#### Exterior Design and HMI

<a href="Process1.jpg" data-lightbox="myimage" data-title="My caption">
    <img src="Process1.jpg" alt="Process1.jpg"/>
</a>

<a href="HMI1.jpg" data-lightbox="myimage" data-title="My caption">
    <img src="HMI1.jpg" alt="HMI1.jpg"/>
</a>

The design of the car exterior and its interaction prototype were developed based on the interior physical model. The wheel design is inspired by the drive wheel of a film projector. To highlight the mobile cinema concept, the car uses a gentle shape with large curvature door panels. The interaction interface emulates Google's Material Design aesthetic, adding depth and a sense of dimension to the UI.

#### Innovation in Viewing Modes

The vehicle uses electrochromic glass for the front windshield and roof, transforming these large areas into screens for viewing and entertainment. In addition to the indoor viewing mode, Ride to Movie can also flip the short-focus projection from the roof outwards for outdoor viewing, transforming multiple cars in a line into a mobile cinema.

#### Target Demographics

Aimed at the family market, this car takes into account the evolving needs of the younger generation (the Z generation) who are likely to have their families by 2035. It addresses the limitations of traditional cinemas and private theaters, and the high cost of setting up home theaters, offering a unique solution for family entertainment.


### Prototype Development
<a href="Prototype1.jpg" data-lightbox="myimage" data-title="My caption">
    <img src="Prototype1.jpg" alt="Prototype1.jpg"/>
</a>


Two 23.8-inch touch screens are used as the control screens in the prototype, with a smartphone as the steering wheel. An Xbox Kinect camera is used for gesture interaction detection, and a high-transparency acrylic board covered with electrochromic film simulates the front windshield and roof glass. The overall interaction prototype and device interconnection are mainly implemented using the JavaScript P5.js library.4