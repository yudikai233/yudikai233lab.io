---
title: Pocket Gym & Following designs
weight: 30
---

# Pocket Gym & Following designs

<br>

{{<youtube 3QGcJKLsgko>}}

<a href="Main.png" data-lightbox="myimage" data-title="My caption">
    <img src="Main.png" alt="Main.png"/>
</a>

## Overview


The Pocket Gym is an innovative approach to home fitness, designed to address various challenges associated with traditional resistance exercise equipment. It targets issues such as bulky size, complicated weight adjustment processes, and the potential danger of solo training. The design replaces conventional weight blocks with an electronically controlled resistance system, which allows the device to be reduced in size for portable convenience, giving it its moniker, "Pocket Gym". Together with its instant plug-and-play resistance control module and an array of workout expansion accessories, users can get real-time feedback from the pocket screen and a dedicated mobile application, truly realizing systematic training at any time and any place.

---
## About

- Red dot Concept award 2021
- Golden Pin award 2021

---
## Process & Detailed Intro

### Background

Various factors such as pandemics, wars, and policy changes have led to an increasingly diverse range of functional requirements for home environments in today's social context, which includes the need for home fitness exercises. Aerobic exercise equipment such as treadmills, rowing machines, and elliptical trainers are highly popular, forming a sizable market. However, for anaerobic (mainly resistance training, e.g., bench press, deadlifts, overhead press) exercises, home products are predominantly lightweight and small-resistance items like dumbbells, resistance bands, and resistance ropes. Heavy and bulky equipment such as barbells, Smith machines, and cable machines has not become mainstream in home fitness products due to their high safety, price, and spatial costs.

Starting with Keniser's pneumatic resistance training equipment in the United States, innovative categories in home fitness, including wall-mounted home gyms like Tonal and aerobic training smart mirrors like Mirror, have continuously emerged. From 2019 to 2022, the home fitness product track, led by Fiture's smart fitness mirror, gradually gained favor from capital in China. In the first half of 2022, an array of new digital resistance home fitness products, such as Speed Diance by Speed Circumstances and Unitree Pump by Unitree, sprung up like mushrooms after the rain. The battlefield for the home fitness scene in China has officially shifted back to hardware, represented by software ecosystems like Keep.


### Start Point: Pascal
{{<youtube 1aTxl4egWKs>}}<br>

### Pocket Gym Design

<a href="Sketches.png" data-lightbox="myimage" data-title="My caption">
    <img src="Sketches.png" alt="Sketches.png"/>
</a>

The Pocket Gym utilizes an electronic motor for resistance adjustment, significantly reducing the size of the equipment while maintaining enough resistance to meet the general fitness needs. Various training expansion accessories can be attached for different exercises, and feedback is provided in real-time through a pocket screen or mobile application.




#### Usage Scenarios & Ergonomics

With a suction cup that works on any flat surface, any wall or floor can become a fulcrum for exercises ranging from dumbbell curls to cable flyes. Regardless of whether you are indoors or outdoors, any flat surface can be transformed into a personal gym.

<a href="User.png" data-lightbox="myimage" data-title="My caption">
    <img src="User.png" alt="User.png"/>
</a>

Suction cups can be arranged on walls or floors at certain distances, coupled with the motor-driven resistance rope, easily achieving different widths for cable machine training modes.

#### Modularity & Details
The top of the device has a replaceable grip module, which can be chosen from resistance bands, triangular handles, barbell rods, and more. The mid-section screen control module uses a magnetic attachment to the device. With only one control module needed per pair of devices, users can conveniently switch between left and right-hand controls. The extended rope at the bottom connects to the suction cup via a carabiner and protective casing.



<a href="1.png" data-lightbox="myimage" data-title="My caption">
    <img src="1.png" alt="1.png"/>
</a>

The suction cups can be placed on any flat surface, including walls and floors. The thread head at the top of the device is translucent, allowing the color of the LED light below to be visible for user pairing. The rope, powered by a motor-driven winder, can extend up to 1.5 meters. The suction cup is tightened by vacuuming the air cap.

### Second-Round Concept Design

<a href="Sketches2.png" data-lightbox="myimage" data-title="My caption">
    <img src="Sketches2.png" alt="Sketches2.png"/>
</a>

The barbell version can be fixed between two walls or a door frame, serving as a pull-up bar. Side rings allow for a variety of exercises, such as cable chest fly, lat pull-downs, etc. The middle part of the bar can be detached for exercises such as bench press, deadlift, and squats. In its fixed state, the compact size and digital resistance adjustment enable it to fit seamlessly into any residential environment without occupying significant space. The bar can be carried onto a plane or train, making it an excellent companion for outdoor resistance training or exercise during travel or business trips.

<a href="Secondesign1.png" data-lightbox="myimage" data-title="My caption">
    <img src="Secondesign1.png" alt="Secondesign1.png"/>
</a>
<a href="Secondesign2.png" data-lightbox="myimage" data-title="My caption">
    <img src="Secondesign2.png" alt="Secondesign2.png"/>
</a>
<a href="Secondesign3.png" data-lightbox="myimage" data-title="My caption">
    <img src="Secondesign3.png" alt="Secondesign3.png"/>
</a>

#### R&D

<a href="RD11.png" data-lightbox="myimage" data-title="My caption">
    <img src="RD11.png" alt="RD11.png"/>
</a>

<a href="RD2.png" data-lightbox="myimage" data-title="My caption">
    <img src="RD2.png" alt="RD2.png"/>
</a>

